<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChannelRepository")
 */
class Channel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $discription;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Provider", inversedBy="channels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $provider;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Program", mappedBy="channel")
     */
    private $updated_at;

    public function __construct()
    {
        $this->updated_at = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDiscription(): ?string
    {
        return $this->discription;
    }

    public function setDiscription(?string $discription): self
    {
        $this->discription = $discription;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    public function setProvider(?Provider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * @return Collection|Program[]
     */
    public function getUpdatedAt(): Collection
    {
        return $this->updated_at;
    }

    public function addUpdatedAt(Program $updatedAt): self
    {
        if (!$this->updated_at->contains($updatedAt)) {
            $this->updated_at[] = $updatedAt;
            $updatedAt->setChannel($this);
        }

        return $this;
    }

    public function removeUpdatedAt(Program $updatedAt): self
    {
        if ($this->updated_at->contains($updatedAt)) {
            $this->updated_at->removeElement($updatedAt);
            // set the owning side to null (unless already changed)
            if ($updatedAt->getChannel() === $this) {
                $updatedAt->setChannel(null);
            }
        }

        return $this;
    }
}
